"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

require("./HTDivider.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HTDivider = function HTDivider() {
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "ht-divider"
  });
};

var _default = HTDivider;
exports.default = _default;